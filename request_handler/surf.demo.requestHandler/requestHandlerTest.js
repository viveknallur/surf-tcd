var express = require("express"),  
    app = express(),
    bodyParser  = require("body-parser"),
    methodOverride = require("method-override");
	 amqp = require('amqplib/callback_api');
    fs = require("fs");	
    mqtt    = require('mqtt');
	 monk = require('monk');
    
app.use(bodyParser.urlencoded({ extended: false }));  
app.use(bodyParser.json());  
app.use(methodOverride());

var router = express.Router();

var subscriber  = mqtt.connect('mqtt://localhost');	
subscriber.on('connect', function () {
	subscriber.subscribe('response');
});


router.post('/serviceRequest', function(req, res) {  
	var request=req.body;
	var data=JSON.parse(fs.readFileSync("./jobId.json", 'utf8'));
	data["jobId"] = data["jobId"] + 1;
	request["jobId"] = 'surf-serviceRequest-'+new Date().getTime()+data["jobId"];
	var publisher  = mqtt.connect('mqtt://localhost');	
	publisher.on('connect', function () {
		console.log(JSON.stringify(request));
		publisher.publish('discovery', JSON.stringify(request));
	});
	fs.writeFile("./jobId.json", JSON.stringify(data), function(err){
      if(err) {
      	   return console.log(err);
		}
   });
	res.end(JSON.stringify(request["jobId"]));
});

router.post('/fakeSDN', function(req, res) {  
	var request=req.body;
	console.log('request for SDN');
	res.end(JSON.stringify(request));
});

router.get('/floodPredictionService/description', function(req, res) {  
	var service=JSON.parse(fs.readFileSync("./services/FloodPredictionService.json", 'utf8'));
	res.end(JSON.stringify(service));
});

router.get('/bestRouteService/description', function(req, res) {  
	var service=JSON.parse(fs.readFileSync("./services/bestRouteService.json", 'utf8'));
	res.end(JSON.stringify(service));
});


router.get('/routingService/description', function(req, res) {  
	var service=JSON.parse(fs.readFileSync("./services/routeService.json", 'utf8'));
	res.end(JSON.stringify(service));
});

router.get('/airPollution/description', function(req, res) {  
	var service=JSON.parse(fs.readFileSync("./services/AirPollutionService.json", 'utf8'));
	res.end(JSON.stringify(service));
});

router.get('/wsdnServices', function(req, res) {
    var db = monk('localhost:27017/surf');
    var collection = db.get('services');
    collection.find({"provider":"wsdn"}, function(err, services){
        if (err) throw err;
      	res.json(services);
    });
})

router.get('/middlewareServices', function(req, res) {
    var db = monk('localhost:27017/surf');
    var collection = db.get('services');
    collection.find({"provider":"middleware"}, function(err, services){
        if (err) throw err;
      	res.json(services);
    });
})

app.use(router);

app.listen(3000, function() {  
  console.log("Node server running on http://localhost:3000/");
});


subscriber.on('message', function (topic, message) {
  // message is Buffer 
  console.log(message.toString());
  subscriber.end();
});

