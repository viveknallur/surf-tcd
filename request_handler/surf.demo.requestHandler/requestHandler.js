var express = require("express"),
    app = express(),
    bodyParser  = require("body-parser"),
    methodOverride = require("method-override");
         amqp = require('amqplib/callback_api');
    fs = require("fs");
    mqtt    = require('mqtt');
         monk = require('monk');

app.use(bodyParser.urlencoded({ extended: false }));
//app.use(bodyParser.json());
app.use(methodOverride());

app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});



var router = express.Router();

var subscriber  = mqtt.connect('mqtt://localhost');
subscriber.on('connect', function () {
        subscriber.subscribe('response/+');
});


router.post('/serviceRequest', function(req, res) {

//      console.log("New request");
//      console.log(req.body);

        var request=req.body;
        request = JSON.stringify(request);
        request = JSON.parse(request);
        request = Object.keys(request)[0];
        request = JSON.parse(request);
        console.log(request);
        console.log("---------------");

        var data=JSON.parse(fs.readFileSync("./jobId.json", 'utf8'));
        data["jobId"] = data["jobId"] + 1;

        var jobID = 'surfServiceRequest'+ new Date().getTime()+ data["jobId"];

        console.log("JobID: " + jobID);

        request["jobId"] = jobID;

        console.log(data);
        console.log("+++++++++++++++");
		        var publisher  = mqtt.connect('mqtt://localhost');
        publisher.on('connect', function () {
                console.log("BEFORE PUBLISH");
//              console.log(JSON.stringify(request));
                publisher.publish('discovery', JSON.stringify(request));
//              publisher.publish('discovery', request);
        });
        fs.writeFile("./jobId.json", JSON.stringify(data), function(err){
                if(err) {
                        return console.log(err);
                }
        });


        console.log(request);

        console.log(JSON.stringify(request["jobId"]));
        res.send(JSON.stringify(request["jobId"]));
});

router.post('/fakeSDN', function(req, res) {
        var request=req.body;
        res.end(JSON.stringify(request));
});

router.get('/floodPredictionService/description', function(req, res) {  
	var service=JSON.parse(fs.readFileSync("./services/FloodPredictionService.json", 'utf8'));
	res.end(JSON.stringify(service));
});

router.get('/bestRouteService/description', function(req, res) {  
	var service=JSON.parse(fs.readFileSync("./services/bestRouteService.json", 'utf8'));
	res.end(JSON.stringify(service));
});


router.get('/routingService/description', function(req, res) {  
	var service=JSON.parse(fs.readFileSync("./services/routeService.json", 'utf8'));
	res.end(JSON.stringify(service));
});

router.get('/airPollution/description', function(req, res) {  
	var service=JSON.parse(fs.readFileSync("./services/AirPollutionService.json", 'utf8'));
	res.end(JSON.stringify(service));
});

router.get('/wsdnServices', function(req, res) {
    var db = monk('localhost:27017/surf');
    var collection = db.get('services');
    collection.find({"provider":"wsdn"}, function(err, services){
        if (err) throw err;
        res.json(services);
    });
})
router.get('/middlewareServices', function(req, res) {
    var db = monk('localhost:27017/surf');
    var collection = db.get('services');
    collection.find({"provider":"middleware"}, function(err, services){
        if (err) throw err;
        res.json(services);
    });
})

app.use(router);

app.listen(3000, function() {
  console.log("Node server running on http://localhost:3000/");
});


subscriber.on('message', function (topic, message) {
  // message is Buffer
  console.log(message.toString());
});