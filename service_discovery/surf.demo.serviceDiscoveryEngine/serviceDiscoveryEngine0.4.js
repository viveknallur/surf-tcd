//MODULES
var mqtt    = require('mqtt');
    LinkedList = require('singly-linked-list');
	 Client = require('node-rest-client').Client;
    fs = require("fs");	
	 ip = require('ip');
    request = require('sync-request');
	 sleep = require('sleep');
    express = require("express"),  
    app = express(),
    bodyParser  = require("body-parser"),
    methodOverride = require("method-override");


app.use(bodyParser.urlencoded({ extended: false }));  
app.use(bodyParser.text({ type: 'text/plain' }))
app.use(methodOverride());

var router = express.Router();

//LOGIC
var Messenger = require('./logic/Messenger.js');
var Extractor = require('./logic/Extractor.js');
var Retrieval = require('./logic/Retrieval.js');
var Matchmaker = require('./logic/Matchmaker.js');
var Overlay = require('./logic/Overlay.js');

var subscriber  = mqtt.connect('mqtt://localhost');

subscriber.on('connect', function () {
	console.log('*****LISTENING FOR DISCOVERY MESSAGE*****');  
	subscriber.subscribe('discovery');
});

subscriber.on('message', function (topic, message) {
	console.log('*****RECEIVING MESSAGE*****');
	console.log('+++++Message+++++');
	var message=JSON.parse(message);
	console.log(JSON.stringify(message));	
	console.log('*****STARTING SERVICE DISCOVERY*****');		
	console.log('*****EXTRACTING INFORMATION FROM REQUEST*****');
	console.log('+++++JobID+++++');
	var jobId = message["jobId"];
	console.log(jobId);
	//Extracting request parameters
	var functionalities = Extractor.extractFunctionalities(message.request.functionalities);
	var inputs = Extractor.extractInputs(message.request.inputs);
	var outputs = Extractor.extractOutputs(message.request.output);
	var qos = Extractor.extractQoSParameters(message.request.QoSParameters);
	var qoi = Extractor.extractQoIParameters(message.request.QoIParameters);
	discoveryAsync(jobId,functionalities,inputs,outputs,qos,qoi);
	setTimeout(function () {
  		flowsSync(jobId,functionalities,inputs,outputs,qos,qoi);
	}, 3000);
});

function discoveryAsync(jobId,functionalities,inputs,outputs,qos,qoi){
	try{	
		//Creating process Structure
		console.log('*****CREATING PROCESS STRUCTURE*****');
		fs.mkdirSync('./process/'+jobId);
		var domains = '{"domains":{';
		for(var i = 0; i<functionalities.getSize(); i++){
			domains = domains + '"'+functionalities.findAt(i).data.value+'":0';
			if(i!=functionalities.getSize()-1)domains = domains + ',';
		}
		domains = domains + '}}';
		var fd = fs.openSync('./process/'+jobId+'/domains.json', 'a');
		fs.writeSync(fd, domains,'utf8');
		fs.closeSync(fd);
		
		var d = '{"services":[]}';
		fd = fs.openSync('./process/'+jobId+'/webServices.json', 'a');
		fs.writeSync(fd, d,'utf8');
		fs.closeSync(fd);

		fd = fs.openSync('./process/'+jobId+'/wsdnServices.json', 'a');
		fs.writeSync(fd, d,'utf8');
		fs.closeSync(fd);

		fd = fs.openSync('./process/'+jobId+'/aspServices.json', 'a');
		fs.writeSync(fd, d,'utf8');
		fs.closeSync(fd);

		fd = fs.openSync('./process/'+jobId+'/middlewareServices.json', 'a');
		fs.writeSync(fd, d,'utf8');
		fs.closeSync(fd);

		//Getting services from Overlay
		for(var i = 0; i<functionalities.getSize(); i++){
			console.log('*****GET SERVICES FROM OVERLAY*****');
			console.log("Searching by domain:"+functionalities.findAt(i).data.value);			
			var domain = functionalities.findAt(i).data.value;
			fs.readFile('./overlay/'+domain+'.json', 'utf8', function (err, data) {
		  		if (err) {
      			return console.log(err);
    			}
				Retrieval.getServicesFromOverlay(data,jobId,qos,qoi)
			});
		}

		//Getting services from ASPs
		console.log('*****GET SERVICES FROM ASPs*****');
		Retrieval.getDescriptionsAutonomousServices(functionalities,jobId,qos,qoi);

		//Getting services from Middleware
		console.log('*****GET SERVICES FROM Middleware*****');
		Retrieval.getDescriptionsMiddlewareServcies(domain,jobId);	
	}
	catch(err){
		return console.error("Error at asynchronous service discovery: " + err);
	}
}

function flowsSync(jobId,functionalities,inputs,outputs,qos,qoi){
	try{
		//Creating flows
		console.log('*****CREATING FLOWS*****');
		var flows = new LinkedList(); 
		flows = createFlowsSync(functionalities,inputs,outputs,qos,qoi,flows,jobId,0,0);
		
		//Ordering flows
		console.log('*****ORDERING FLOWS*****');
		var orderedFlows=new LinkedList();
		for(var i=0;i<flows.getSize();i++){
			var flow = flows.findAt(i).data;
			var orderedFlow = new LinkedList();
			for(var j=(flow.getSize()-1);j>=0;j--){
				if('first' in flow.findAt(j).data)delete flow.findAt(j).data.first;
				orderedFlow.insert(flow.findAt(j).data);			
			}
			orderedFlows.insert(orderedFlow);
		}		
		
		//Merging parallel flows		
		orderedFlows = Matchmaker.merge(orderedFlows,functionalities,jobId);
		if(orderedFlows.getSize()>0){
			//Creating composition message
			var compositionMessage = Messenger.createCompositionMessage(jobId,orderedFlows,inputs);
			//Sending composition message
			Messenger.publishCompositionMessage('compose',compositionMessage);
		}
		else console.log('*****NO SERVICES AVAILABLE TO SATISFY THE REQUEST*****');
		console.log('*****DISCOVERY FINISHED*****');
	}
	catch(err){
		return console.error("Error at synchronous flows creation: " + err);
	}
}

function createFlowsSync(functionalities,inputs,outputs,qos,qoi,flows,jobId,lap,b){
	try{
		if(flows.isEmpty()){
			console.log('*****STARTING FLOWS CREATION*****');
			//console.log(lap);
			var retrievedServices = Retrieval.getServices(functionalities, outputs, qos, qoi,jobId);
			for(var i = 0; i<retrievedServices.getSize(); i++){
				var flow = new LinkedList();
				var inputsService = new LinkedList();
				for(var input  in retrievedServices.findAt(i).data.inputs){
					var inp = new Input(retrievedServices.findAt(i).data.inputs[input].name,retrievedServices.findAt(i).data.inputs[input].type);
					inputsService.insert(inp);
				}
				for(var j=0; j<inputsService.getSize(); j++){
					var inp = JSON.parse(inputsService.findAt(j));
				}
				for(var j=0; j<inputs.getSize(); j++){
					var inp = JSON.parse(inputs.findAt(j));
					}
				if(Matchmaker.io(inputs,inputsService)==1){
					b++;
					retrievedServices.findAt(j).data.first=1;
					flow.insert(retrievedServices.findAt(i).data);
					flows.insert(flow);
				}
				else{
					flow.insert(retrievedServices.findAt(i).data);
					flows.insert(flow);
				}
			}
			if(b!=retrievedServices.getSize()){
				flows = createFlowsSync(functionalities,inputs,outputs,qos,qoi,flows,jobId,1,b);
			}
		}
		else{
			var l = lap;
			l++;
			var newFlows = new LinkedList();
			for(var i = 0; i<flows.getSize(); i++){
				var flow = flows.findAt(i).data;
				var service = flow.findAt(flow.getSize()-1).data;
				if(!('first' in service)){
					var tempOutputs = new LinkedList();
					for(var inputDescription in service.inputs){
						var inp = service.inputs[inputDescription];					
						if(!('value' in inp)){
							var out = new Output(inp["name"],inp["type"]);
							tempOutputs.insert(out);
						}
					}
					var retrievedServices = Retrieval.getServices(functionalities, tempOutputs, qos, qoi,jobId);
					for(var j = 0; j<retrievedServices.getSize(); j++){
						var f = new LinkedList();
						for (var t = 0; t<flow.getSize(); t++)f.insert(flow.findAt(t).data);
						var inputsService = new LinkedList();
						for(var input  in retrievedServices.findAt(j).data.inputs){
							var inp = new Input(retrievedServices.findAt(j).data.inputs[input].name,retrievedServices.findAt(j).data.inputs[input].type);
							inputsService.insert(inp);
						}
						for(var x=0; x<inputsService.getSize(); x++){
							var inp = JSON.parse(inputsService.findAt(x));
						}
						for(var x=0; x<inputs.getSize(); x++){
							var inp = JSON.parse(inputs.findAt(x));
						}
							
						if(Matchmaker.io(inputs,inputsService)==1){
							b++;
							retrievedServices.findAt(j).data.first=1;
							f.insert(retrievedServices.findAt(j).data);
							newFlows.insert(f);
						}
						else{
							f.insert(retrievedServices.findAt(j).data);
							newFlows.insert(f);
						}
					}
				}
				else{
					newFlows.insert(flow);				
				}
			}
			if(b!=newFlows.getSize()){
				flows = createFlowsSync(functionalities,inputs,outputs,qos,qoi,newFlows,jobId,l,b);
			}
			else flows = newFlows;
		}	
		return flows;
	}
	catch(err){
		return console.error("Error Creating Flows: " + err);
	}
}

router.post('/wsdnDiscovery', function(req, res) {  
	var output ='{"services": [';
	var message = req.body.split('T');
	var jobId = message[0];
	var domain = message[1];
	var routes = message[2].replace('[','').replace(']','').split(',H,');
	for(var i=0;i<routes.length;i++){
		output = output + '{"route":['
		routes[i]=routes[i].replace(',H','');
		var coordinates= routes[i].split(',');
		for(var j=0;j<coordinates.length;j++){
			output=output + Retrieval.getWsdnServices(jobId,domain,coordinates[j]);
			if(j<coordinates.length-1)output=output + ',';
			else output = output + ']}';
		}
		if(i<routes.length-1)output=output + ',';
	}
	output = output + ']}';
	res.end(output);
});

app.use(router);

app.listen(3001, function() {  
  console.log("Node server running on http://localhost:3001/");
});
