var mqtt    = require('mqtt');
	 mongoClient = require('mongodb').MongoClient;
	 LinkedList = require('singly-linked-list');
	 ObjectId = require('mongodb').ObjectID;
	 Client = require('node-rest-client').Client;

var subscriber  = mqtt.connect('mqtt://localhost');

subscriber.on('connect', function () {
	console.log('*****LISTENING FOR DISCOVERY MESSAGE*****');  
	subscriber.subscribe('discovery');
});

subscriber.on('message', function (topic, message) {
	console.log('*****RECEIVING MESSAGE*****');	
	discover(message);
});

function publish(topic, message){
	var publisher  = mqtt.connect('mqtt://localhost');	
	publisher.on('connect', function () {
//		publisher.publish('compose', JSON.stringify(compose));
		publisher.publish(topic, message);
	});
}

function discover(req){
	try{	
		console.log('*****STARTING DISCOVERY*****');	
		var message=JSON.parse(req);
		console.log(JSON.stringify(message));	
		var servicesRequest = new LinkedList();
		for(var service in message.request.services) {
			var ser = message.request.services[service];
			ser["QoSParameters"]=message.request["QoSParameters"];
			ser["QoIParameters"]=message.request["QoIParameters"];
			ser["jobId"]=message["jobId"];
			servicesRequest.insert(ser);		
		}
		for(var i = 0; i<servicesRequest.getSize(); i++){
			lookupByType(servicesRequest.findAt(i));
			lookupByIO(servicesRequest.findAt(i));
		}
	}
	catch(err){
		return console.error("Error at discover operation: " + err);
	}
}

function lookupByType(ser){
	console.log('*****LOOKUP BY TYPE*****');	
	var service=JSON.parse(ser);
	console.log(JSON.stringify(service));	
	mongoClient.connect("mongodb://localhost:27017/surf", function(err, db) {
  		if(!err) {
			db.collection('services').find({"type.physicalProcess":service["type"]}).toArray(function(err, results){
				if (err){
					return console.error("Error at service lookup: " + err);
				}
				else{
					results.forEach(function(result) {
						if(result["provider"]=="wsdn"){
							console.log(result);
							result["QoSParameters"]=service["QoSParameters"];
							result["QoIParameters"]=service["QoIParameters"];
							sdnRequest(result);
						}
					});
				}
			});
		}
	});
}

function sdnRequest(service){
	try{	
		console.log('*****SENDING REQUEST TO SDN*****');	
		var client = new Client();
		var args ={
		 data: service,
   	 headers: { "Content-Type": "application/json" }
		};
		console.log(JSON.stringify(args));	  
		client.post("http://localhost:3000/fakeSDN",args, function (data, response) {
			console.log('***RESPONSE FROM SDN*****');	
			console.log(JSON.parse(data));		
		});
	}
	catch(err){
		return console.error("Error at sdn request: " + err);
	}
}



