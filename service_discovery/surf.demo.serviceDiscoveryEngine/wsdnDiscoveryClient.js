var express = require("express"),  
    app = express(),
    bodyParser  = require("body-parser"),
    methodOverride = require("method-override");
    fs = require("fs");	
	 requestAsync = require('request');

var dataFile = fs.readFileSync('./input', 'utf8');
console.log(dataFile);

requestAsync({
    url: 'http://localhost:3001/wsdnDiscovery/', //URL to hit
    method: 'POST',
    headers: {
        'Content-Type': 'text/plain',
    },
    body: dataFile
}, function(error, response, body){
    if(error) {
        console.log(error);
    } else {
        console.log(body);
    }
});

