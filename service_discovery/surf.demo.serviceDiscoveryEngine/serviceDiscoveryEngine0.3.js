//MODULES
var mqtt    = require('mqtt');
    LinkedList = require('singly-linked-list');
	 Client = require('node-rest-client').Client;
    fs = require("fs");	
	 ip = require('ip');
    request = require('sync-request');

//LOGIC
var Messenger = require('./logic/Messenger.js');
var Extractor = require('./logic/Extractor.js');
var Retrieval = require('./logic/Retrieval.js');
var Matchmaker = require('./logic/Matchmaker.js');

var subscriber  = mqtt.connect('mqtt://localhost');

subscriber.on('connect', function () {
	console.log('*****LISTENING FOR DISCOVERY MESSAGE*****');  
	subscriber.subscribe('discovery');
});

subscriber.on('message', function (topic, message) {
	console.log('*****RECEIVING MESSAGE*****');
	console.log('+++++Message+++++');
	var message=JSON.parse(message);
	console.log(JSON.stringify(message));	
	discoverySync(message);
});

function discoverySync(message){
	try{	
		console.log('*****STARTING SERVICE DISCOVERY*****');		
		console.log('*****EXTRACTING INFORMATION FROM REQUEST*****');
		console.log('+++++JobID+++++');
		var jobId = message["jobId"];
		console.log(jobId);
		
		//Extracting request parameters
		var functionalities = Extractor.extractFunctionalities(message.request.functionalities);
		var inputs = Extractor.extractInputs(message.request.inputs);
		var outputs = Extractor.extractOutputs(message.request.output);
		var qos = Extractor.extractQoSParameters(message.request.QoSParameters);
		var qoi = Extractor.extractQoIParameters(message.request.QoIParameters);
		
		//Discovering service flows
		var flows = new LinkedList(); 
		flows = createFlowsSync(functionalities,inputs,outputs,qos,qoi,flows,0);
		console.log('*****PRINTING FLOWS*****');
		for(var j = 0; j<flows.getSize(); j++){
			console.log('+++++Flow ' + (j+1) + '+++++');
			var flow = flows.findAt(j).data;
			for(var i = 0; i<flow.getSize(); i++){
				console.log('+++++Service ' + (i+1) + '+++++');
				var service = flow.findAt(i).data;			
				console.log(JSON.stringify(service));
			}
		}
		
		var orderedFlows=new LinkedList();
		for(var i=0;i<flows.getSize();i++){
			var flow = flows.findAt(i).data;
			var orderedFlow = new LinkedList();
			for(var j=(flow.getSize()-1);j>=0;j--){
				orderedFlow.insert(flow.findAt(j).data);			
			}
			orderedFlows.insert(orderedFlow);
		}

		
		//Creating composition message
		var compositionMessage = Messenger.createCompositionMessage(jobId,orderedFlows,inputs);

		//Sending composition message
		Messenger.publishCompositionMessage('compose',compositionMessage);

		console.log('*****DISCOVERY FINISHED*****');
	}
	catch(err){
		return console.error("Error at synchronous service discovery: " + err);
	}
}

function createFlowsSync(functionalities,inputs,outputs,qos,qoi,flows,lap){
	try{
		if(flows.isEmpty()){
			console.log('*****STARTING FLOWS CREATION*****');
			var retrievedServices = getServices(functionalities, outputs, qos, qoi);
			console.log('Retrieved services'+retrievedServices.getSize());
			for(var i = 0; i<retrievedServices.getSize(); i++){
				var flow = new LinkedList();
				var b = 0;	
				for(var inputDescription in retrievedServices.findAt(i).data.inputs){
					var inp = retrievedServices.findAt(i).data.inputs[inputDescription];
					for(var x = 0; x<inputs.getSize(); x++){
						if((inp["name"].toUpperCase()==inputs.findAt(x).data["name"].toUpperCase())&&(inp["type"].toUpperCase()==inputs.findAt(x).data["type"].toUpperCase())){
							b = 1;					
						}
					}	
				}
				if(b == 1){
					flow.insert(retrievedServices.findAt(i).data);
					flows.insert(flow);
				}
				else{
					flow.insert(retrievedServices.findAt(i).data);
					flows.insert(flow);
				}
			}
			if(b==0){
				console.log('Before recursion:'+flows.getSize());
				flows = createFlowsSync(functionalities,inputs,outputs,qos,qoi,flows,1);
			}
		}
		else{
			var l = lap;
			console.log(''+l+' recursion:'+flows.getSize());
			l++;
			var b = 0;
			var newFlows = new LinkedList();
			for(var i = 0; i<flows.getSize(); i++){
				var flow = flows.findAt(i).data;
				var service = flow.findAt(flow.getSize()-1).data;
				var tempOutputs = new LinkedList();
				for(var inputDescription in service.inputs){
					var inp = service.inputs[inputDescription];					
					var out = new Output(inp["name"],inp["type"]);
					tempOutputs.insert(out);
				}
				var retrievedServices = getServices(functionalities, tempOutputs, qos, qoi);
				console.log('Retrieved services'+retrievedServices.getSize());
				for(var j = 0; j<retrievedServices.getSize(); j++){
					var f = new LinkedList();
					for (var t = 0; t<flow.getSize(); t++)f.insert(flow.findAt(t).data);
					for(var inputDescription in retrievedServices.findAt(j).data.inputs){
						var inp = retrievedServices.findAt(j).data.inputs[inputDescription];
						b = 0;
						for(var x = 0; x<inputs.getSize(); x++){
							if((inp["name"].toUpperCase()==inputs.findAt(x).data["name"].toUpperCase())&&(inp["type"].toUpperCase()==inputs.findAt(x).data["type"].toUpperCase())){
								b=1;								
							}
						}	
					}
					if(b == 1){
						console.log(retrievedServices.findAt(j).data);
						f.insert(retrievedServices.findAt(j).data);
						console.log(f);
						console.log(newFlows.getSize());
						newFlows.insert(f);
						console.log(newFlows.getSize());
					}
					else{
						f.insert(retrievedServices.findAt(j).data);
						newFlows.insert(f);
					}
				}
			}
			if(b==0){
				flows = createFlowsSync(functionalities,inputs,outputs,qos,qoi,newFlows,l);
			}
			else flows = newFlows;
		}	
		return flows;
	}
	catch(err){
		return console.error("Error Creating Flows: " + err);
	}
}

function getServices(functionalities,outputs,qos,qoi){
		var services = new LinkedList();		
		console.log('-----Retrieving Web Services-----');
		var webServices = new LinkedList();
		webServices = Retrieval.getWebServices(functionalities, outputs, qos, qoi);
		for(var i = 0; i<webServices.getSize(); i++){
			services.insert(webServices.findAt(i).data);
		}
		/*console.log('-----Retrieving WSDN Services-----');
		var wsdnServices = new LinkedList();
		wsdnServices = Retrieval.getWsdnServices(functionalities, outputs, qos, qoi);
		for(var i = 0; i<wsdnServices.getSize(); i++){
			services.insert(wsdnServices.findAt(i).data);
		}		
		console.log('-----Retrieving Autonomous Services-----');
		var autonomousServices = new LinkedList();
		autonomousServices = Retrieval.getAutonomousServices(functionalities, outputs, qos, qoi);
		for(var i = 0; i<autonomousServices.getSize(); i++){
			services.insert(autonomousServices.findAt(i).data);
		}
		/*console.log('-----Retrieving Middleware Services-----');
		var middlewareServices = new LinkedList();
		middlewareServices = Retrieval.getMiddlewareServices(functionalities, outputs, qos, qoi);
		for(var i = 0; i<middlewareServices.getSize(); i++){
			services.insert(middlewareServices.findAt(i).data);
		}*/
		console.log(''+services.getSize());
		return services;
}

function lookupByType(ser){
	console.log('*****LOOKUP BY TYPE*****');	
	var service=JSON.parse(ser);
	console.log(JSON.stringify(service));	
	mongoClient.connect("mongodb://localhost:27017/surf", function(err, db) {
  		if(!err) {
			db.collection('services').find({"type.physicalProcess":service["type"]}).toArray(function(err, results){
				if (err){
					return console.error("Error at service lookup: " + err);
				}
				else{
					results.forEach(function(result) {
						if(result["provider"]=="wsdn"){
							console.log(result);
							result["QoSParameters"]=service["QoSParameters"];
							result["QoIParameters"]=service["QoIParameters"];
							Messenger.sdnRequest(result);
						}
					});
				}
			});
		}
	});
}

