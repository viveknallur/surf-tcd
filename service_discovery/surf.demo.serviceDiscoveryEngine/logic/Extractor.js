//MODULES
var LinkedList = require('singly-linked-list');

//MODEL
var Functionality = require('../model/Functionality.js');
	 Input = require('../model/Input.js');
	 Output = require('../model/Output.js');
	 QoSParameter = require('../model/QoSParameter.js');
	 QoIParameter = require('../model/QoIParameter.js');

module.exports = {
	extractFunctionalities: function (functionalities) {
		console.log('+++++Functionalities+++++');	
		var f = new LinkedList();
		for(var functionality in functionalities) {
			var fun = new Functionality(functionality,functionalities[functionality]);
			f.insert(fun);		
		}
		for(var j = 0; j<f.getSize(); j++){
			var fun = JSON.parse(f.findAt(j));
			console.log(JSON.stringify(fun));			
		}
		return f;
	},
	extractInputs: function (inputs) {
		console.log('+++++Inputs+++++');	
		var i = new LinkedList();
		for(var input in inputs) {
			var inp = new Input(inputs[input].name,inputs[input].type,inputs[input].value);
			i.insert(inp);		
		}
		for(var j = 0; j<i.getSize(); j++){
			var inp = JSON.parse(i.findAt(j));
			console.log(JSON.stringify(inp));			
		}
		return i;	
	},
	extractOutputs: function(outputs) {
		console.log('+++++Outputs+++++');	
		var o = new LinkedList();
		//for(var output in outputs) {
			var out = new Output(outputs.name,outputs.type);
			o.insert(out);		
		//}
		for(var j = 0; j<o.getSize(); j++){
			var out = JSON.parse(o.findAt(j));
			console.log(JSON.stringify(out));			
		}
		return o;
	},
	extractQoSParameters: function(QoSParameters) {
		console.log('+++++QoS Parameteres+++++');	
		var q = new LinkedList();
		for(var QoS in QoSParameters) {
			var qosp = new QoSParameter(QoS,QoSParameters[QoS]);
			q.insert(qosp);		
		}
		for(var j = 0; j<q.getSize(); j++){
			var qosp = JSON.parse(q.findAt(j));
			console.log(JSON.stringify(qosp));			
		}
		return q;	
	},
	extractQoIParameters: function(QoIParameters) {
		console.log('+++++QoI Parameteres+++++');	
		var q = new LinkedList();
		for(var QoI in QoIParameters) {
			var qoip = new QoIParameter(QoI,QoIParameters[QoI]);
			q.insert(qoip);		
		}
		for(var j = 0; j<q.getSize(); j++){
			var qoip = JSON.parse(q.findAt(j));
			console.log(JSON.stringify(qoip));			
		}
		return q;	
	}
};
