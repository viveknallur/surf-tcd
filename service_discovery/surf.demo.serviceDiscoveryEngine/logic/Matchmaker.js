//MODULES
var LinkedList = require('singly-linked-list');
	 fs = require("fs");	

module.exports = {
	io: function (list1,list2) {
		var res=0;
		var matched=0;
		for(var i=0; i<list1.getSize(); i++){
			for(var j=0; j<list2.getSize(); j++){
				var ls1 = JSON.parse(list1.findAt(i));
				var ls2 = JSON.parse(list2.findAt(j));
				if(ls1.type.toUpperCase()==ls2.type.toUpperCase()&&ls1.name.toUpperCase()==ls2.name.toUpperCase()){
					matched ++;
					break;					
				}
			}
		}
		if(matched==0) res = 0;		
		if(matched==list1.getSize() && matched==list2.getSize()) res = 1;
		if(matched>0 && matched<list1.getSize() && matched==list2.getSize()) res = 2;
		if(matched>0 && matched<list1.getSize() && matched>list2.getSize()) res = 3;
		if(matched>0 && matched<list1.getSize() && matched<list2.getSize()) res = 4;
		return res;
	},
	location: function (lon1,lon2,lat1,lat2) {
		var res=0;
		if(lon1.trim()==lon2.trim()&&lat1.trim()==lat2.trim())res = 1;
		else res = 0;
		return res;
	},
	merge: function (flows,functionalities,jobId) {
		var newFlows = new LinkedList();
		if(flows.getSize()>1){
			for(var i = 0; i<flows.getSize(); i++){
				var b1 = 0;				
				var flow1 = new LinkedList();
				var temp = flows.findAt(i).data;
				var tempFlows = new LinkedList();
				for(var z=0;z<temp.getSize();z++)flow1.insert(temp.findAt(z).data);
				temp = new LinkedList();
				for(var j=i+1; j<flows.getSize(); j++){
					var b2=0;
					var newFlow = new LinkedList();
					var flow2 = new LinkedList();
					temp=flows.findAt(j).data;
					for(var z=0;z<temp.getSize();z++)flow2.insert(temp.findAt(z).data);
					temp = new LinkedList();
					for(var x = 0; x<flow1.getSize(); x++){
						if(x<flow2.getSize()){
							var s1 = flow1.findAt(x).data;
							var s2 = flow2.findAt(x).data;
							if(this.similarity(s1,s2)==1){
								newFlow.insert(s1);
								b2=1;
							}
							if(this.similarity(s1,s2)==2){
								var ns = this.newService(s1,s2)
								ns['domain']=''+s1['domain']+','+s2['domain'];
								newFlow.insert(ns);
								var middlewareService = this.getMiddlewareService('sceAggregator',jobId);
								middlewareService['inputs']=JSON.parse('['+JSON.stringify(s1['output']) +','+ JSON.stringify(s2['output'])+']');
								middlewareService['output']=s1['output'];
								newFlow.insert(middlewareService);
								b2=1;
							}
							if(this.similarity(s1,s2)==0){
								b2=0;
								break;
							}
						}
						else break;
					}
					if(b2==1){
						tempFlows.insert(newFlow);
						b1=1;
					}
				}			
				if(b1==0){
					tempFlows.insert(flow1);
				}
				for(var e=0; e<tempFlows.getSize();e++){
					newFlows.insert(tempFlows.findAt(e).data);
				}
				tempFlows = new LinkedList();
			}
		}
		else{
			var resFlows = new LinkedList();
			for(var i = 0; i<flows.getSize(); i++){
				if(this.relevantFlow(flows.findAt(i).data,functionalities))resFlows.insert(flows.findAt(i).data);
			}
			return resFlows;
		}
		
		var resFlows = new LinkedList();
		for(var i = 0; i<newFlows.getSize(); i++){
			if(this.relevantFlow(newFlows.findAt(i).data,functionalities))resFlows.insert(newFlows.findAt(i).data);
		}
		return resFlows;
	},
	newService: function (s1,s2) {
		var ser = '[';
		ser = ser + JSON.stringify(s1) + ',';
		ser = ser + JSON.stringify(s2);
		ser = ser + ']';
		return JSON.parse(ser);
	},
	similarity: function (s1,s2) {
		var res=0;
		var d1 = s1['domain'];
		var d2 = s2['domain'];
		var t1 = s1['serviceType'];
		var t2 = s2['serviceType'];
		var url1 = s1['serviceURL'];
		var url2 = s2['serviceURL'];
		var inputs1 = new LinkedList();
		for(var input in s1.inputs){
			var inp = s1.inputs[input];					
			if(!('value' in inp)){
				var newInp = new Input(inp["name"],inp["type"]);
				inputs1.insert(newInp);
			}
		}
		var outputs1 = new LinkedList();
		var out = new Output(s1.output.name,s1.output.type);
		outputs1.insert(out);		
	
		var inputs2 = new LinkedList();
		for(var input in s2.inputs){
			var inp = s2.inputs[input];					
			if(!('value' in inp)){
				var newInp = new Input(inp["name"],inp["type"]);
				inputs2.insert(newInp);
			}
		}
		var outputs2 = new LinkedList();
		out = new Output(s2.output.name,s2.output.type);
		outputs2.insert(out);		
		if(d1==d2 && url1==url2 && this.io(inputs1,inputs2)==1 && this.io(outputs1,outputs2)==1)return 1;
		if(d1!=d2 && this.io(inputs1,inputs2)==1 && this.io(outputs1,outputs2)==1)return 2;
		return 0;
	},
	relevantFlow: function (flow,functionalities) {
		var domains = '';
		for(var i = 0; i<flow.getSize();i++){
			domains = domains + ',' + flow.findAt(i).data.domain;	
		}
		domains=domains.toUpperCase();
		for(var i = 0; i<functionalities.getSize();i++){
			if(!domains.includes(functionalities.findAt(i).data['value'].toUpperCase()))return false;
		}
		return true;
	},
	getMiddlewareService: function (type,jobId) {
		var middlewareServices = new LinkedList();
		var middlewareService;
		var services = JSON.parse(fs.readFileSync('./process/'+jobId+'/middlewareServices.json', 'utf8'))['services'];
		for(var service in services){
			if(services[service].serviceType=='sceAggregator'){
				return services[service];
			}
		}
	}
};
