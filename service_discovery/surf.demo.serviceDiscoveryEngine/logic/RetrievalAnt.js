//MODULES
var LinkedList = require('singly-linked-list');
	 fs = require("fs");	

//LOGIC
var Messenger = require('../logic/Messenger.js');
var Matchmaker = require('../logic/Matchmaker.js');
var Output = require('../model/Output.js');

module.exports = {
	getServicesFromOverlay:function(data,jobId,qos,qoi){
		var dataFromOverlay = JSON.parse(data);
		var domain = dataFromOverlay['domain'];
		var services = dataFromOverlay['services'];
		var wsdnServices = new LinkedList();
		var webServices = new LinkedList();
		console.log(services);
		for(var service in services){
			console.log(services[service]);
			var ser = services[service];
			if(ser['provider']=='wsdn'){
				wsdnServices.insert(ser);
			}
			if(ser['provider']=='webService'){
				webServices.insert(ser);
			}
		}
		
		if(wsdnServices.getSize()>0){
			console.log("En las tetas de Alicia que ricas que estan, apretarlas rico, cabalga Alicia tetona que rico que estuvo");
			getWsdnServices(domain,qos,qoi, function (err, data) {
		  		if (err) {
   	  			return console.log(err);
  				}
				updateDomainsRetrieved(domain,jobId)
			});
		}
		
		if(webServices.getSize()>0){
			console.log("En las tetas de Alicia que ricas que estan, apretarlas rico");
			getWebServices(webServices,qos,qoi, function (err, data) {
			  	if (err) {
   	   		return console.log(err);
   	 		}
				updateDomainsRetrieved(domain,jobId)
			});
		}
	},
	getWebServices: function (functionalities,outputs,qos,qoi) {
		var webServices = new LinkedList();
		var files = fs.readdirSync('./services/');
		for(var file in files){
			console.log('-----Files in directory-----'+files[file]);
			var service = JSON.parse(fs.readFileSync('./services/'+files[file], 'utf8'));
			if(service["serviceType"].toUpperCase()=='WEB SERVICE'){
				var outputsDescription = new LinkedList();
				var out = new Output(service.output.name,service.output.type);
				outputsDescription.insert(out);
				if(Matchmaker.matchMaking(outputsDescription,outputs)==1){
						webServices.insert(service);
				}
			}	
		}
		console.log(webServices.getSize());
		return webServices;	
	},
	getWsdnServices: function(functionalities,outputs,qos,qoi) {
		var wsdnServices = new LinkedList();	
		var res = request('GET', 'http://localhost:3000/wsdnServices');
		var services = JSON.parse(res.getBody());
		for(var ser in services){
			var service = services[ser];
			var outputsDescription = new LinkedList();
			var out = new Output(service.output.name,service.output.type);
			outputsDescription.insert(out);
			if(Matchmaker.matchMaking(outputsDescription,outputs)==1){
				if(!qos.isEmpty()){
					var qosser='{';
					for(var x = 0; x<qos.getSize(); x++){
						qosser = qosser + '"'+qos.findAt(x).data["name"]+'":'+ qos.findAt(x).data["value"];
							if(x<qos.getSize()-1)qosser = qosser + ',';
					}
					qosser = qosser + '}';
					service["QoSParameters"]=JSON.parse(qosser.replace("\'",""));
				}
				if(!qoi.isEmpty()){
					var qoiser='{';
					for(var x = 0; x<qoi.getSize(); x++){
						qoiser = qoiser + '"'+qoi.findAt(x).data["name"]+'":'+ qoi.findAt(x).data["value"];
						if(x<qoi.getSize()-1)qoiser = qoiser + ',';	
					}
					qoiser = qoiser + '}';
					service["QoIParameters"]=JSON.parse(qoiser.replace("\'",""));
				}
				response = Messenger.sdnRequest(service);
				wsdnServices.insert(response);
			}
			
		}
		return wsdnServices;
	},
	getMiddlewareServices: function(functionalities,outputs,qos,qoi) {
		var middlewareServices = new LinkedList();	
		var res = request('GET', 'http://localhost:3000/middlewareServices');
		var services = JSON.parse(res.getBody());
		for(var ser in services){
			var service = services[ser];
			console.log('*****HERE*****');
			console.log(JSON.stringify(service));	
			var outputsDescription = new LinkedList();
			var out = new Output(service.output.name,service.output.type);
			outputsDescription.insert(out);
			if(Matchmaker.matchMaking(outputsDescription,outputs)==1){			
				if(!qos.isEmpty()){
					var qosser='{';
					for(var x = 0; x<qos.getSize(); x++){
						qosser = qosser + '"'+qos.findAt(x).data["name"]+'":'+ qos.findAt(x).data["value"];
						if(x<qos.getSize()-1)qosser = qosser + ',';
					}
					qosser = qosser + '}';
					service["QoSParameters"]=JSON.parse(qosser.replace("\'",""));
				}
				if(!qoi.isEmpty()){
					var qoiser='{';
					for(var x = 0; x<qoi.getSize(); x++){
						qoiser = qoiser + '"'+qoi.findAt(x).data["name"]+'":'+ qoi.findAt(x).data["value"];
						if(x<qoi.getSize()-1)qoiser = qoiser + ',';	
					}
					qoiser = qoiser + '}';
					service["QoIParameters"]=JSON.parse(qoiser.replace("\'",""));
				}
				middlewareServices.insert(service);
			}
			
		}
		return middlewareServices;
	},
	getAutonomousServices: function(functionalities,outputs,qos,qoi) {
		var autonomousServices = new LinkedList();
		/*try{	
			console.log('*****SENDING MESSAGE TO ASPs*****');
			topic='aspDiscovery';
			message='discovery';
			var publisher  = mqtt.connect('mqtt://localhost');	
			publisher.on('connect', function () {
				publisher.publish(topic, message);
			});
		}
		catch(err){
			return console.error("Error sending message: " + err);
		}
		
		var subscriber  = mqtt.connect('mqtt://localhost');
		subscriber.on('connect', function () {
			console.log('*****LISTENING FOR ASPs RESPONSE*****');  
			subscriber.subscribe('aspDiscovery');
		});

		subscriber.on('message', function (topic, message) {
			console.log('*****RECEIVING MESSAGE FROM ASP*****');
			console.log('+++++Message+++++');
			var message=JSON.parse(message);
			console.log(JSON.stringify(message));	
			discoverySync(message);
		});*/
		return autonomousServices;	
	},
	updateDomainsRetrieval:function(domain,jobId){
		var domainsFile = JSON.parse(fs.readFileSync('./process/'+jobId+'/domains.json', 'utf8'));
		var domains = domainsFile['domains'];
		for(var d in domains){
			if(d==domain){
				console.log(d);
				console.log(domain);
				domains[d] = 1;
			}
		}
		domainsFile['domains']=domains;
		console.log(domainsFile);
		fs.writeFileSync('./process/'+jobId+'/domains.json',JSON.stringify(domainsFile));
	}
};
