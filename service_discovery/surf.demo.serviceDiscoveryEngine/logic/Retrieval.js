//MODULES
var mqtt    = require('mqtt');
    LinkedList = require('singly-linked-list');
	 fs = require("fs");	

//LOGIC
var Messenger = require('../logic/Messenger.js');
var Matchmaker = require('../logic/Matchmaker.js');
var Output = require('../model/Output.js');

var subscriberASP  = mqtt.connect('mqtt://localhost');
subscriberASP.on('connect', function () {
	console.log('*****LISTENING FOR ASPs MESSAGES*****');
	subscriberASP.subscribe('aspDiscovery/response/+');
});

module.exports = {
	getServicesFromOverlay:function(data,jobId,qos,qoi){
		try{
			var dataFromOverlay = JSON.parse(data);
			var domain = dataFromOverlay['domain'];
			var services = dataFromOverlay['services'];
			var wsdnServices = new LinkedList();
			var webServices = new LinkedList();
			for(var service in services){
				var ser = services[service];
				if(ser['provider']=='webService'){
					webServices.insert(ser);
				}			
				if(ser['provider']=='wsdn'){
					wsdnServices.insert(ser);
				}
			}
			if(webServices.getSize()>0){
				this.getDescriptionsWebServices(webServices,qos,qoi,jobId,domain);
			}
			if(wsdnServices.getSize()>0){
				this.getDescriptionsWsdnServices(wsdnServices,qos,qoi,jobId,domain);
			}
		}catch(err){
			console.error("Error getting services from overlay: " + err);				
		}
	},
	getDescriptionsWebServices: function (webServices,qos,qoi,jobId,domain) {
		try{
			var data='';
			for(var i=0; i<webServices.getSize();i++){
				var service = webServices.findAt(i).data;
				var descriptionUrl = service['endpoint'];
				try{	
					console.log('*****SENDING REQUEST FOR SERVICE DESCRIPTION*****');	
					console.log(descriptionUrl);
					var res = request('GET', descriptionUrl);
					var ser = res.getBody('utf8');
					data = data + ser;
					if(i<webServices.getSize()-1) data = data + ',';
				}
				catch(err){
					console.error("Error sending request for service description: " + err + ' ' + descriptionUrl);
					if(i==webServices.getSize()-1){
						data = data.slice(0, -1);
					}
				}
			}
			if(data!=''){
				var dataFile = JSON.parse(fs.readFileSync('./process/'+jobId+'/webServices.json', 'utf8'));
				var ws = JSON.stringify(dataFile['services']);
				if(ws=='[]')ws = '['+data+']';
				else ws = ws + '***,' + data+']';
				ws = ws.replace(']***,',',');
				dataFile['services']=JSON.parse(ws);
				fs.writeFileSync('./process/'+jobId+'/webServices.json',JSON.stringify(dataFile));
				this.updateDomainsRetrieved(domain,jobId);
			}
		}
		catch(err1){
			console.error("Error getting web services: " + err1);
		}
	},
	getDescriptionsWsdnServices: function(wsdnServices,qos,qoi,jobId,domain) {
		try{
			var data='';
			for(var i=0; i<wsdnServices.getSize();i++){
				var qs='{"QoSParameters":{';
				for(var q=0;q<qos.getSize();q++){
					qs = qs +'"'+qos.findAt(q).data.name+'":'+qos.findAt(q).data.value;
					if(q<qos.getSize()-1)qs = qs + ',';
				}
				qs = qs + '}}';			
				wsdnServices.findAt(i).data["QoSParameters"]=JSON.parse(qs)["QoSParameters"];
				var qi='{"QoIParameters":{';
				for(var q=0;q<qoi.getSize();q++){
					qi = qi +'"'+ qoi.findAt(q).data.name+'":'+qoi.findAt(q).data.value;
					if(q<qoi.getSize()-1)qi = qi + ',';
				}			
				qi = qi + '}}';
				wsdnServices.findAt(i).data["QoIParameters"]=JSON.parse(qi)["QoIParameters"];
				var service = JSON.stringify(wsdnServices.findAt(i).data);
				data = data + service;
				if(i<wsdnServices.getSize()-1) data = data + ',';
			}
			if(data!=''){
				var dataFile = JSON.parse(fs.readFileSync('./process/'+jobId+'/wsdnServices.json', 'utf8'));
				var ws = JSON.stringify(dataFile['services']);
				if(ws=='[]')ws = '['+data+']';
				else ws = ws + '***,' + data+']';
				ws = ws.replace(']***,',',');
				dataFile['services']=JSON.parse(ws);
				fs.writeFileSync('./process/'+jobId+'/wsdnServices.json',JSON.stringify(dataFile));
				this.updateDomainsRetrieved(domain,jobId);	
			}
		}
		catch(err){
			return console.error("Error getting wsdn services: " + err);
		}
	},
	getDescriptionsMiddlewareServcies: function(domain,jobId) {
		try{
			var files = fs.readdirSync('./middlewareServices/');
			var data='';	
			for(var file in files){
				var service = JSON.parse(fs.readFileSync('./middlewareServices/'+files[file], 'utf8'));
				if(service['serviceType']=='wsdnSD'){
					var inputs = service['inputs'];
					for(var input in inputs){
						if(inputs[input].name=='domain'){
							inputs[input].value=domain;
						}
						if(inputs[input].name=='jobId'){
							inputs[input].value=jobId;
						}
					}
					service['inputs']=inputs;
				}
				data = data + JSON.stringify(service) + ',';
			}
			if(data!=''){
				data = data + '***';
				data = data.replace(',***','');
				var dataFile = JSON.parse(fs.readFileSync('./process/'+jobId+'/middlewareServices.json', 'utf8'));
				var ws = JSON.stringify(dataFile['services']);
				if(ws=='[]')ws = '['+data+']';
				else ws = ws + '***,' + data+']';
				ws = ws.replace(']***,',',');
				dataFile['services']=JSON.parse(ws);
				fs.writeFileSync('./process/'+jobId+'/middlewareServices.json',JSON.stringify(dataFile));
			}
		}
		catch(err){
			return console.error("Error getting middleware services: " + err);
		}
	},
	getDescriptionsAutonomousServices: function(functionalities,jobId,qos,qoi) {
		message='{"domain":[';
		var domain ='';
		for(var i = 0; i<functionalities.getSize(); i++){
			domain = functionalities.findAt(i).data.value;
			message =message + '{"name":"'+domain+'"}';
			if(i<functionalities.getSize()-1)message = message + ',';
		}
		message = message + '],"jobId":"'+jobId+'"}';
		try{	
			console.log('*****SENDING MESSAGE TO ASPs*****');
			topic='aspDiscovery/request';
			var publisher  = mqtt.connect('mqtt://localhost');	
			publisher.on('connect', function () {
				publisher.publish(topic, message);
			});
			subscriberASP.on('message', function (topic, message) {
				if(topic.includes(jobId)){				
					console.log('*****RECEIVING MESSAGE FROM ASP*****');
					console.log('+++++Message+++++');
					var message=JSON.parse(message);
					var data = JSON.stringify(message);
					domain = message['domain'];
					if(data!=''){
						var dataFile = JSON.parse(fs.readFileSync('./process/'+jobId+'/aspServices.json', 'utf8'));
						var ws = JSON.stringify(dataFile['services']);
						if(ws=='[]')ws = '['+data+']';
						else ws = ws + '***,' + data+']';
						ws = ws.replace(']***,',',');
						dataFile['services']=JSON.parse(ws);
						fs.writeFileSync('./process/'+jobId+'/aspServices.json',JSON.stringify(dataFile));	
					}
				}else console.log('*****IGNORING MESSAGE FOR OTHER JOB*****');
			});
			this.updateDomainsRetrieved(domain,jobId);
		}
		catch(err){
			return console.error("Error sending message to ASPs: " + err);
		}
	},
	updateDomainsRetrieved:function(domain,jobId){
		try{		
			var domainsFile = JSON.parse(fs.readFileSync('./process/'+jobId+'/domains.json', 'utf8'));
			var domains = domainsFile['domains'];
			for(var d in domains){
				if(d==domain){
					domains[d] = 1;
				}
			}
			domainsFile['domains']=domains;
			fs.writeFileSync('./process/'+jobId+'/domains.json',JSON.stringify(domainsFile));
		}
		catch(err){
			return console.error("Error updating domains retrieved: " + err);
		}
	},
	getServices:function(functionalities, outputs, qos, qoi,jobId){
		var services = new LinkedList();		
		console.log('-----Retrieving Services for Flows Creation-----');
		console.log('-----Retrieving Available Web Services-----');
		var webServices = new LinkedList();
		webServices = this.getWebServices(functionalities, outputs, qos, qoi,jobId);
		for(var i = 0; i<webServices.getSize(); i++){
			services.insert(webServices.findAt(i).data);
		}
		console.log('-----Retrieving Available Middleware Services-----');
		var middlewareServices = new LinkedList();
		middlewareServices = this.getMiddlewareServices(functionalities, outputs, qos, qoi,jobId);
		for(var i = 0; i<middlewareServices.getSize(); i++){
			services.insert(middlewareServices.findAt(i).data);
		}
		console.log('-----Retrieving Available ASPs Services-----');
		var aspServices = new LinkedList();
		aspServices = this.getAspServices(functionalities, outputs, qos, qoi,jobId);
		for(var i = 0; i<aspServices.getSize(); i++){
			services.insert(aspServices.findAt(i).data);
		}
		return services;
	},
	getWebServices: function (functionalities,outputs,qos,qoi,jobId) {
		var webServices = new LinkedList();
		var services = JSON.parse(fs.readFileSync('./process/'+jobId+'/webServices.json', 'utf8'))['services'];
		for(var service in services){
			var outputsDescription = new LinkedList();
			var out = new Output(services[service].output.name,services[service].output.type);
			outputsDescription.insert(out);
			if(Matchmaker.io(outputsDescription,outputs)==1){
					webServices.insert(services[service]);
			}
		}
		return webServices;	
	},
	getMiddlewareServices: function (functionalities,outputs,qos,qoi,jobId) {
		var middlewareServices = new LinkedList();
		var services = JSON.parse(fs.readFileSync('./process/'+jobId+'/middlewareServices.json', 'utf8'))['services'];
		for(var service in services){
			if(services[service].serviceType!='sceAggregator'){
				var outputsDescription = new LinkedList();
				var out = new Output(services[service].output.name,services[service].output.type);
				outputsDescription.insert(out);
				if(Matchmaker.io(outputsDescription,outputs)==1){
						middlewareServices.insert(services[service]);
				}
			}
		}
		return middlewareServices;	
	},
	getAspServices: function (functionalities,outputs,qos,qoi,jobId) {
		var aspServices = new LinkedList();
		var services = JSON.parse(fs.readFileSync('./process/'+jobId+'/aspServices.json', 'utf8'))['services'];
		for(var service in services){
			var outputsDescription = new LinkedList();
			var out = new Output(services[service].output.name,services[service].output.type);
			outputsDescription.insert(out);
			if(Matchmaker.io(outputsDescription,outputs)==1){
					aspServices.insert(services[service]);
			}
		}
		return aspServices;	
	},
	getWsdnServices: function (jobId,domain,coordinates) {
		var wsdnServices = '';
		var coor = coordinates.split('|');
		var lon = coor[0].trim();
		var lat = coor[1].trim();
		var services = JSON.parse(fs.readFileSync('./process/'+jobId+'/wsdnServices.json', 'utf8'))['services'];
		for(var service in services){
			var s = JSON.stringify(services[service]);
			s = '{"Service":'+s+'}';
			var lonService = services[service].location.coordinates.x;
			var latService = services[service].location.coordinates.y;
			if(Matchmaker.location(lon,lonService,lat,latService)==1){	
					ser = Messenger.sdnRequest(s)['Service'];
					wsdnServices = wsdnServices + '{"lat": "'+lat+'",';
					wsdnServices = wsdnServices + '"lon": "'+lon+'",';
					wsdnServices = wsdnServices + '"serviceDescription": '+JSON.stringify(ser)+'}'
			}
		}
		return wsdnServices;	
	}
};
