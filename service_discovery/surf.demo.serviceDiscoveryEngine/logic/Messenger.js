var mqtt    = require('mqtt');
    LinkedList = require('singly-linked-list');
    Client = require('node-rest-client').Client;

module.exports = {
	publishCompositionMessage: function (topic, message) {
		try{	
			console.log('*****SENDING MESSAGE*****');
			console.log('+++++Topic+++++');
			console.log(topic);
			console.log('+++++Message+++++');	
			console.log(message);
			var publisher  = mqtt.connect('mqtt://localhost');	
			publisher.on('connect', function () {
				publisher.publish(topic, message);
			});
		}
		catch(err){
			return console.error("Error sending message: " + err);
		}
	},
	createCompositionMessage: function (jobId,flows,inputs) {
		try{	
			console.log('*****CREATING MESSAGE*****');
			var message = '{"jobId": "'+jobId+'","responseHandler": {"address" : "tcp://192.168.1.110:1883"},"initialInputs" : [';
			for(var j=0; j<inputs.getSize();j++){
				var i = JSON.parse(inputs.findAt(j));
				message = message + '{"name":"'+i.name+'","value":"'+i.value+'"}';
				if(j<inputs.getSize()-1)message = message + ","
			}
			message = message + '],"flows":[';
			for(var j = 0; j<flows.getSize(); j++){
				var flow = flows.findAt(j).data;
				message = message + '{"flowId" : "'+j+'","services": [';
				for(var i = 0; i<flow.getSize(); i++){
					message = message + '{"serviceId" : "'+ i +'",';
					var s = 0;
					for(var ser in flow.findAt(i).data){
						if(ser>=0)s++;
					}
					if(s>0)message= message + '"split":"'+s+'",';
					message = message + '"service":'+JSON.stringify(flow.findAt(i).data);			
					message = message + "}"
					if(i<flow.getSize()-1)message = message + ","
				}
				message = message + ']}';				
				if(j<flows.getSize()-1)message = message + ","
			}
			message = message + ']}';
			return message;
		}
		catch(err){
			return console.error("Error Creating Message: " + err);
		}
	},
	sdnRequest: function(service) {
		try{	
			var s = JSON.parse(service);
			console.log('*****SENDING REQUEST TO SDN*****');	
			var res = request('POST', 'http://192.168.2.5:8888/query', {
				json: s
			});
			var ser = JSON.parse(res.getBody('utf8'));
			return ser;
		}
		catch(err){
			return console.error("Error at sdn request: " + err);
		}
	}
};


