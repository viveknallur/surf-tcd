General Requirements:

- Install mqtt (mosquitto)
	$sudo apt-add-repository ppa:mosquitto-dev/mosquitto-ppa
	$sudo apt-get update
	$sudo apt-get install mosquitto
	$sudo apt-get install libmosquitto-dev
	$sudo apt-get install mosquitto-clients
	$sudo service mosquitto status 
	
- Install mongodb
	$ sudo apt-get install mongodb
	
- Create database schema and test data
	$ mongo			//Starts MongoDB shell version
	> use surf		//Creates surf database
	> db.services.insert({"transactionId" : "2", "provider" : "wsdn", "domain" : "flooding", "inputs" : { "routeEdges" : { "type" : "string", "description" : "The output is the list of route edges separated by comma" } }, 
	"outputs" : { "edgesFlooding" : { "type" : "json", "description" : "The output is the flooding in each edge" } }, "state" : "Active", 
	"type" : { "direction" : "Sensing", "physicalProcess" : "WaterLevel", "dataType" : "String", "unit" : "cm" }, "samplingPeriod" : "Rate of sampling the physical process", 
	"aggregation" : { "function" : "avg", "perNode" : "true", "window" : "10" }, "dataReporting" : { "condition" : { "type" : "greaterThan", "threshold" : "100" }, "dataPeriod" : "ignored" }, 
	"_comment" : "Comment about water level", "location" : { "coordinates" : { "x" : "Longitude", "y" : "Latitude", "z" : "Altitude" }, "region" : "(x_i, y_i) i=[1,4]" }, 	
	"temporality" : { "start" : "2016/08/29 12:00:00", "duration" : "120" }, "QoSParameters" : { "reliability" : 25, "exampleS" : 35 }, "QoIParameters" : { "accuracy" : 15, "exampleI" : 85 } });

- Install npm
	$ sudo apt-get install npm
- Install node
	$ sudo npm cache clean -f
	$ sudo npm install -g n
	$ sudo n stable
	$ node -v
