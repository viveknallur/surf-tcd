var mqtt    = require('mqtt');
	 mongoClient = require('mongodb').MongoClient;

	 ObjectId = require('mongodb').ObjectID;

var subscriber  = mqtt.connect('mqtt://localhost');

subscriber.on('connect', function () {
	console.log('*****LISTENING FOR REGISTRATION MESSAGE*****');  
	subscriber.subscribe('registration');
});

subscriber.on('message', function (topic, message) {
	console.log('*****RECEIVING MESSAGE*****');	
	register(message);
});

function register(ser){
	console.log('*****STARTING REGISTRATION*****');	
	var message=JSON.parse(ser);
	console.log(JSON.stringify(message));	
	//for(var service in message) {
		storeService(message["Service"]);		
	//}
}

function storeService(service){
	try{	
		console.log('*****STORING SERVICE*****');	
		console.log(JSON.stringify(service));	
		service["provider"]="wsdn";	
		mongoClient.connect("mongodb://localhost:27017/surf", function(err, db) {
  			if(!err) {
				db.collection('services').update({"transactionId":service["transactionId"]},service,{upsert: true},function(err, results){
					if (err){
						var publisher  = mqtt.connect('mqtt://localhost');	
						publisher.on('connect', function () {
							publisher.publish('registrationResponse', 'Error at insert/update operation:' + err);
						});
					}
					else{
						var publisher  = mqtt.connect('mqtt://localhost');	
						publisher.on('connect', function () {
							publisher.publish('registrationResponse', 'success');
						});
					}
					});
			}
			else{
				var publisher  = mqtt.connect('mqtt://localhost');	
					publisher.on('connect', function () {
						publisher.publish('registrationResponse', 'Error connecting mongodb: ' + err);
				});
			}
		});
	}
	catch(err){
		var publisher  = mqtt.connect('mqtt://localhost');	
			publisher.on('connect', function () {
				publisher.publish('registrationResponse', 'Exception storing service: ' + err);
		});
	}
}
