var fs = require("fs");	
    mqtt    = require('mqtt');
 

var publisher  = mqtt.connect('mqtt://localhost');	
publisher.on('connect', function () {
	var data = JSON.parse(fs.readFileSync("./service.json", 'utf8'));
	console.log(JSON.stringify(data));
	publisher.publish('registration', JSON.stringify(data));
});


var subscriber  = mqtt.connect('mqtt://localhost');	
subscriber.on('connect', function () {
  subscriber.subscribe('registrationResponse');
});

subscriber.on('message', function (topic, message) {
	console.log('***RECEIVING MESSAGE***');	
	console.log(message.toString());
});
